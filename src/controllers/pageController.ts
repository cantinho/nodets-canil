import { Request, Response } from "express";

export const home = (req: Request, res: Response) => {
    // res.send('home no controller');
    res.render('pages/page', {
        banner: {
            title: 'Todos os Animais',
            background: 'allanimals.jpg'
        }
    });
}
export const dogs = (req: Request, res: Response) => {
    res.render('pages/page', {
        banner: {
            title: 'Todos os Dog',
            background: 'banner_dog.jpg'
        }
    });
}
export const cats = (req: Request, res: Response) => {
    res.render('pages/page', {
        banner: {
            title: 'Todos os Gatos',
            background: 'banner_cat.jpg'
        }
    });
}    
export const fishes = (req: Request, res: Response) => {
    res.render('pages/page', {
        banner: {
            title: 'Todos os Peixes',
            background: 'banner_fish.jpg'
        }
    });
}